﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OptionController : MonoBehaviour
{

		public List<QA> Answers = new List<QA> ();
		private QA qa;

		public void CreateObjects ()
		{
				qa = gameObject.AddComponent<QA> ();
				qa.EnglishName = "Shirts";
				qa.SpanishName = "Camisas";
				qa.EnglishNumber = "Five";
				qa.SpanishNumber = "Cinco";
				Answers.Add (qa);
		}

		// Use this for initialization
		void Start ()
		{
				CreateObjects ();
		}

}
