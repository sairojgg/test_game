﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionFunction : MonoBehaviour
{

		public string value;
		private int level = 0;
		public QA qa;
		private OptionController oc;
		private ArturoController ac;
	
		// Update is called once per frame
		public void CheckAnswer (Text text)
		{
				oc.CreateObjects ();
				value = oc.Answers [level].SpanishNumber;
				if (value.Equals (text.text))
						ac.correct = true;
				else
						ac.correct = false;
				ac.CheckAnswer (ac.correct);
		}

		public void setValue (string value)
		{
				this.value = value;

		}

	void Start(){
		qa = gameObject.AddComponent (typeof(QA)) as QA;
		oc = gameObject.AddComponent<OptionController> ();
		ac = gameObject.AddComponent<ArturoController> ();
	}
}
