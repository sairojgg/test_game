﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeText : MonoBehaviour {

	private Text text;

	public void switchText(string newText){
		text = GameObject.FindGameObjectWithTag ("Finish").GetComponent<Text>() as Text;
		text.text = newText;
	}

}
