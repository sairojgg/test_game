﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ArturoController : MonoBehaviour
{

		public AudioClip[] audioClip;
		public bool correct = false;
		protected Animator animator;
		ChangeText ct;
		private bool completed = false;

		void PlaySound (int clip)
		{
				ct = gameObject.AddComponent<ChangeText> ();
				if (clip == 3)
						ct.switchText ("¿Cuantas camisas hay?");
				if (clip == 2)
						ct.switchText ("How many shirts are there?");
				if (clip == 0)
						ct.switchText ("Si. Cinco camisas");
				if (clip == 1)
						ct.switchText ("No. Es el numero cinco");
				GameObject.FindGameObjectWithTag ("Player").audio.clip = audioClip [clip];
				GameObject.FindGameObjectWithTag ("Player").audio.Play ();
				
		}
	
		public void CheckAnswer (bool correct)
		{
				if (correct) {
						StartCoroutine (CorrectAnswer ());
				} else {
						StartCoroutine (IncorrectAnswer ());
				}
		}

		IEnumerator IncorrectAnswer ()
		{
				PlaySound (5);
				yield return new WaitForSeconds (1.0f);
				PlaySound (1);
		}

		IEnumerator CorrectAnswer ()
		{
				completed = true;
				PlaySound (4);
				yield return new WaitForSeconds (1.0f);
				PlaySound (0);
		}

		void Start ()
		{
				StartCoroutine (ArturoWalk ());
				animator = GameObject.FindGameObjectWithTag ("Player").GetComponent<Animator> ();
				audioClip = new AudioClip[6];
				audioClip [0] = Resources.Load ("correct-answer-explain") as AudioClip;
				audioClip [1] = Resources.Load ("incorrect-answer-explain") as AudioClip;
				audioClip [2] = Resources.Load ("question-english") as AudioClip;
				audioClip [3] = Resources.Load ("question-spanish") as AudioClip;
				audioClip [4] = Resources.Load ("bell") as AudioClip;
				audioClip [5] = Resources.Load ("buzzer") as AudioClip;
		}

		void Update ()
		{
				if (completed) {
						StopCoroutine (ArturoWalk ());
						completed = false;
				}
		}

		IEnumerator ArturoWalk ()
		{
				while (true) {
						yield return new WaitForSeconds (12.0f);
						animator.SetBool ("WalkLeft", false);
						animator.SetBool ("WalkRight", true);
						yield return new WaitForSeconds (12.0f);
						animator.SetBool ("WalkRight", false);
						animator.SetBool ("WalkLeft", true);
				}
		}
		
}
